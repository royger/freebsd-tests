#!/bin/sh

set -e

# Partition that contains the root system
partition=4

print_help()
{
	echo "Usage: ./$0 <image path>"
}

if [ $# -ne 1 ]; then
	echo "Inalid number of arguments"
	print_help
	exit 1
fi

disk=`mdconfig -a $1`
mount_point=`mktemp -d`
mount /dev/md0p$partition $mount_point

printf "%s" "-h -S115200" >> $mount_point/boot.config
cat << ENDBOOT >> $mount_point/boot/loader.conf
	boot_serial="YES"
	comconsole_speed="$c{Baud}"
	console="comconsole"
	boot_verbose="YES"
	beastie_disable="YES"
ENDBOOT

umount $mount_point
mdconfig -d -u $disk

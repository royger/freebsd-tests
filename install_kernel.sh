#!/bin/sh

set -e

# Partition that contains the root system
partition=4

print_help()
{
	echo "Usage: $0 <src path> <image path> <extra arguments for buildkernel>"
}

if [ $# -lt 2 ]; then
	echo "Inalid number of arguments"
	print_help
	exit 1
fi

src=$1
shift
image=$1
shift

(
	cd $src
	make -j`sysctl -n hw.ncpu` buildkernel "$@"
)

disk=`mdconfig -a $image`
mount_point=`mktemp -d`
mount /dev/md0p$partition $mount_point

(
	cd $src
	make installkernel DESTDIR=$mount_point
)

umount $mount_point
mdconfig -d -u $disk
